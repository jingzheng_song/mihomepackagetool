#合作开发自动打包脚本。详见 https://wiki.n.miui.com/pages/viewpage.action?pageId=145101278

#获取即将打包的设备名称和tag
device_name=$1
tag_name=$2
current_branch=$3

if [[ $device_name = "" || tag_name = "" ]]; then
#statements
echo '[ERROR] 打包设备名称和tag号不能为空'
exit 0;
fi

cd ../mihomeinternal

if [[ $current_branch = "" ]]; then
    #statements
    git pull --rebase origin master
    branchName=jenkins_$(echo $device_name | tr 'A-Z' 'a-z')
    echo "即将切换到分支：$branchName"

    git branch -D $branchName
    git push origin --delete $branchName
    git checkout -b $branchName
    current_branch=$branchName
else
    git checkout $current_branch
    git pull origin $current_branch
    git merge origin/master
    git status
fi



#拷贝代码到internal/devices
cd ../MiHomePackageTool
./device_configs.py -n $device_name -t $tag_name -b ""
result=$?
if [[ $result > 0 ]]; then
	echo "拷贝 $branchName 分支的代码失败，请检查后重试。"
    exit 0;
fi

#提交

#加入新的代码
read -p "是否不需要手工改动代码? 如果需要，先去修改代码，不要进行任何操作。否则输入'y'：" name
if [[ $name = "y" ]]; then
	cd ../mihomeinternal
    git add .
    git commit -a -m "add $current_branch code"
    #pod install。有时候可能有文件的增删，此处我们pod install一下
    rm -rf MiHome/Pods/Manifest.lock
    cd MiHome && pod install
    git add ..
    git commit -a -m "pod install"

    git push origin $current_branch
fi

#Jenkins打包
read -p "是否确认代码无误，可直接Jenkins打包？(y/n)：" isok
if [[ $isok = "y" ]]; then
echo "打包参数为：delay=0sec&token=zwf_cooperation_build&branch=$current_branch&tag=$tag_name&upload_fir=true&fir_token=ce8c5bc4174a562917c538fc704d90c1"
curl -d "delay=0sec&token=zwf_cooperation_build&branch=$current_branch&tag=$tag_name&upload_fir=true&fir_token=ce8c5bc4174a562917c538fc704d90c1" http://www.mihomejenkins.top:8080/job/Feature1/buildWithParameters
echo "请打开 http://www.mihomejenkins.top:8080/job/Feature1/buildWithParameters 查看"
fi
