#!/usr/bin/env python
#coding=utf-8

#
import os
import sys
import getopt
import fileinput
import re

#获取外面传过来的参数 Lumi 4.27.0
try:
    options,args = getopt.getopt(sys.argv[1:],"n:t:b:", ["name=","tag=","branch="])
except getopt.GetoptError:
    sys.exit()
for name,value in options:
    if name in ("-n","--name"):
        device_name = value
    if name in ("-t","--tag"):
        tag_name = value
    if name in ("-b","--branch"):
        branch_name = value


modules = {'ChuangMi' : 'ssh://git@dev.imilab.com:222/ipc-plug/ipc-plug-ios.git', 'MIOWLDoorRing' : 'git@github.com:MADV360/MiHomePlugin_MIDingLing.git', 'Lumi' : 'git@github.com:lumigit/Mijia-Dailybuild-Lumi.git','ismartalarm':'git@github.com:hualaikeji/miPluginCamera.git','XinAnVehicle':'git@github.com:ZhangPan0825/XinAnVehicle.git','MJCatEye':'git@github.com:derekhuangxu/MJCatEye.git','SimCamCamera':'git@github.com:XingTuZhiKong/SimCamCamera.git','DunCateye':'git@github.com:idunmi/dun-cateye-ios.git','HTPrinter':'git@github.com:Hannto/ht_ios_for_mi.git','MXDevices':'git@github.com:552322771/MXDevices.git','MGCamera':'git@github.com:laughmaker/MGCamera_iOS.git','Xiaovv':'git@github.com:hongshiforgit/Xiaovv.git'}

#获取git地址
if modules.has_key(device_name):
    git_url = modules[device_name]
else:
    inputData = raw_input("请打开./device_configs，在modules里面加入"+device_name+"的git地址后重试，输入任意值退出:")
    if len(inputData)>0:
        sys.exit(1)

print branch_name;

array = git_url.split('/')
last_obj = array[len(array)-1].split('.')

#git clone到本地的文件夹的名称
dir_name = last_obj[0]

#git clone文件所在父目录，保留在本地，方便下次使用（不用git clone了），此目录和mihomeinternal平级
tmp_path = '../operation'

#../opeartion/Mijia-Dailybuild-Lumi  Lumi clone下来的代码目录，判断是否存在。
tmp_dir = tmp_path+'/'+dir_name;

print tmp_dir;

pull_str = "git pull --tags"
dest_name = tag_name

if branch_name != "":
    pull_str = "git fetch origin " + branch_name
    dest_name = branch_name

if os.path.exists(tmp_dir) == True:
    #如果存在，则只需要切换到对应的tag
    tmp_operation1 = 'cd '+ tmp_path + '/' + dir_name + " && " + pull_str;
    print tmp_operation1
    os.system(tmp_operation1)
    
    tmp_operation2 = 'cd '+ tmp_path + '/' + dir_name + " && " + 'git checkout ' + dest_name + ' && git pull origin ' + dest_name;
    print tmp_operation2
    os.system(tmp_operation2)
else:
    #如果不存在，则新建文件夹，git clone对应的tag的代码
    os.system('mkdir '+ tmp_path)
    tmp_operation2 = 'cd ' + tmp_path + " && git clone --branch " + dest_name + " " + git_url
    print tmp_operation2
    os.system(tmp_operation2)

#将代码覆盖Internal/Devices/Lumi里面的文件
device_path = '../mihomeinternal/Internal/Devices/' + device_name
if os.path.exists(device_path) == False:
    os.system('mkdir ' + device_path)

os.system('rm -rf ' + device_path + '/*')
tmp_str = 'cp -rf ' + tmp_path + '/' + dir_name + '/* ' + device_path;
print tmp_str;
os.system(tmp_str)

#修改对应的.podspec中的version为tag_version，source为git@v9.git.n.xiaomi.com:qiaoliwei/mihomeinternal.git
podspec_file = "{0}.podspec".format(device_path+'/'+device_name)
print podspec_file

for line in fileinput.input(podspec_file, inplace=1):
    match_version = re.match(r'\s*s\.version\s*=\s*\"(.*)\"',line) # 修改podspec 的version
    if match_version:
        line =  "  s.version      = \"{0}\"\n".format(tag_name)
    match_source = re.match(r'\s*s\.source\s*=\s*\{(.*)\}',line) #修改podspec 的source
    if match_source:
        git_source = ":git => 'git@v9.git.n.xiaomi.com:qiaoliwei/mihomeinternal.git'"
        line =  "  s.source = {}{},:tag=>s.version.to_s{}".format("{",git_source,"}")
    print line,

sys.exit(0)
