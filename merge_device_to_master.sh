#https://wiki.n.miui.com/pages/viewpage.action?pageId=153391822

#获取即将打包的设备名称和tag
device_name=$1
tag_name=$2

# git checkout master
#git pull origin master

#拷贝代码到internal/devices
if [[ $device_name = "ChuangMi" ]]
then
    ./device_configs.py -n $device_name -t $tag_name -b "imimaster"

else
    ./device_configs.py -n $device_name -t $tag_name -b ""
fi

result=$?
if [[ $result > 0 ]]; then
    exit 0;
fi

cd ../mihomeinternal
branchName=jenkins_$(echo $device_name | tr 'A-Z' 'a-z')_$tag_name
echo "即将切换到:$branchName"
git branch -D $branchName
git push origin --delete $branchName
git checkout -b $branchName
git status

#加入新的代码
read -p "是否不需要手工改动代码? 如果需要，先去修改代码，不要进行任何操作。否则输入y：" name
if [[ $name = "y" ]]; then
    git add .
    git commit -a -m "add $branchName code"
    #pod install。有时候可能有文件的增删，此处我们pod install一下
    cd MiHome && pod install
    git add .
    git commit -a -m "pod install"

    git push origin $branchName
    git checkout master
    git branch -D $branchName
fi
